<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {
    public function index()
    {
        $data['mahasiswa'] = $this->m_mahasiswa->tampil_data();
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('mahasiswa', $data);
        $this->load->view('templates/footer');
    }

    public function tambah_aksi(){
        $data=[
            'nama'       => $this->input->post('nama'),
            'nim'        => $this->input->post('nim'),
            'tgl_lahir'  => $this->input->post('tgl_lahir'),
            'jurusan'    => $this->input->post('jurusan'),
        ];

        $this->m_mahasiswa->input_data($data, 'tb_mahasiswa'); //didalam variabel data array
        redirect('mahasiswa/index'); //ke controller mahasiswa method index
    }

    public function hapus($id){
        $where = array ('id' => $id);
        $this->m_mahasiswa->hapus_data($where, 'tb_mahasiswa');
        redirect ('mahasiswa/index');
    }

    public function edit($id){
        $where = array ('id' => $id);
        $data['mahasiswa'] = $this->m_mahasiswa->edit_data($where, 'tb_mahasiswa')->result();
        
        $this->load->view('templates/header');
        $this->load->view('templates/sidebar');
        $this->load->view('edit', $data);
        $this->load->view('templates/footer');
    }

    public function update(){
        $id     = $this->input->post('id');
        $data=[
            'nama'       => $this->input->post('nama'),
            'nim'        => $this->input->post('nim'),
            'tgl_lahir'  => $this->input->post('tgl_lahir'),
            'jurusan'    => $this->input->post('jurusan'),
        ];
        $where = array(
            'id'         => $id
        );

        $this->m_mahasiswa->update_data($where, $data, 'tb_mahasiswa'); //didalam variabel data array
        redirect('mahasiswa/index'); //ke controller mahasiswa method index
    }
}