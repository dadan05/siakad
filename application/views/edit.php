<div class="content-wrapper">
    <section class="content">
        <?php foreach ($mahasiswa as $mhs) { ?>
            <form method="post" action="<?php echo base_url().'mahasiswa/update'; ?>">
                
                <div class="form-group">
                    <label>Nama Mahasiswa</label>
                    <input type="hidden" name="id" class="form-control" value="<?php echo $mhs->id?>">
                    <input type="text" name="nama" class="form-control" value="<?php echo $mhs->nama?>">
                </div>
                <div class="form-group">
                    <label>NIM</label>
                    <input type="text" name="nim" class="form-control" value="<?php echo $mhs->nim?>">
                </div>
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="date" name="tgl_lahir" class="form-control" value="<?php echo $mhs->tgl_lahir?>">
                </div>
                <div class="form-group">
                    <label>Jurusan</label>
                    <select class="form-control" name="jurusan" value="<?php echo $mhs->jurusan?>">
                                <option>Teknik Informatika</option>
                                <option>Teknik Komputer</option>
                                <option>Teknik Elektro</option>
                                <option>Sistem Informasi</option>
                    </select>
                </div>

                <button type="reset" class="btn btn-danger">Reset</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </form>
        <?php } ?>
    </section>
</div>